![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

A website built with [Hugo], a static website generator built in [Go]. See it at [casho.es].

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

[hugo]: https://gohugo.io
[go]: https://golang.org
[casho.es]: http://casho.es
